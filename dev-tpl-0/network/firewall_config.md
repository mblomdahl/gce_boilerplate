# Custom Firewalls (REST)


## _gb-icmp_ Firewall

    {
      "kind": "compute#firewall",
      "selfLink": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/firewalls/gb-icmp",
      "id": "18007767343816701868",
      "creationTimestamp": "2013-11-24T21:59:13.238-08:00",
      "name": "gb-icmp",
      "description": "ICMP allowed from anywhere",
      "network": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/networks/default",
      "sourceRanges": [
        "0.0.0.0/0"
      ],
      "allowed": [
        {
          "IPProtocol": "icmp"
        }
      ]
    }

_To be continued..._
