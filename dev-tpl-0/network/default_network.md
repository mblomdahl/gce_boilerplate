# Default Network Config (REST)

## Default Network

    {
      "kind": "compute#network",
      "selfLink": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/networks/default",
      "id": "16734111520216240086",
      "creationTimestamp": "2013-09-18T13:44:49.485-07:00",
      "name": "default",
      "description": "Default network for the project",
      "IPv4Range": "10.240.0.0/16",
      "gatewayIPv4": "10.240.0.1"
    }


## Default Firewalls

    {
      "kind": "compute#firewallList",
      "selfLink": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/firewalls",
      "id": "projects/maklarstatistik.se:maklarstatistik-se/global/firewalls",
      "items": [
        {
          "kind": "compute#firewall",
          "selfLink": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/firewalls/default-allow-internal",
          "id": "8214463313784480013",
          "creationTimestamp": "2013-09-18T13:44:50.131-07:00",
          "name": "default-allow-internal",
          "description": "Internal traffic from default allowed",
          "network": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/networks/default",
          "sourceRanges": [
            "10.0.0.0/8"
          ],
          "allowed": [
            {
              "IPProtocol": "tcp",
              "ports": [
                "1-65535"
              ]
            },
            {
              "IPProtocol": "udp",
              "ports": [
                "1-65535"
              ]
            },
            {
              "IPProtocol": "icmp"
            }
          ]
        },
        {
          "kind": "compute#firewall",
          "selfLink": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/firewalls/default-ssh",
          "id": "14439549811071696653",
          "creationTimestamp": "2013-09-18T13:44:50.429-07:00",
          "name": "default-ssh",
          "description": "SSH allowed from anywhere",
          "network": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/networks/default",
          "sourceRanges": [
            "0.0.0.0/0"
          ],
          "allowed": [
            {
              "IPProtocol": "tcp",
              "ports": [
                "22"
              ]
            }
          ]
        }
      ]
    }


## Default Routes

    {
      "kind": "compute#routeList",
      "selfLink": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/routes",
      "id": "projects/maklarstatistik.se:maklarstatistik-se/global/routes",
      "items": [
        {
          "kind": "compute#route",
          "selfLink": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/routes/default-route-6930e9210f7e3f12",
          "id": "5350317612914919934",
          "creationTimestamp": "2013-09-18T13:44:49.676-07:00",
          "name": "default-route-6930e9210f7e3f12",
          "description": "Default route to the Internet.",
          "network": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/networks/default",
          "destRange": "0.0.0.0/0",
          "priority": 1000,
          "nextHopGateway": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/gateways/default-internet-gateway"
        },
        {
          "kind": "compute#route",
          "selfLink": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/routes/default-route-ec9800b364dc2598",
          "id": "18021635177919152567",
          "creationTimestamp": "2013-09-18T13:44:49.676-07:00",
          "name": "default-route-ec9800b364dc2598",
          "description": "Default route to the virtual network.",
          "network": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/networks/default",
          "destRange": "10.240.0.0/16",
          "priority": 1000,
          "nextHopNetwork": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/networks/default"
        }
      ]
    }


