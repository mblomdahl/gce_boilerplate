# REST Config

## Persistent Disk from Image

    POST https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/zones/europe-west1-a/disks?sourceImage=https%3A%2F%2Fwww.googleapis.com%2Fcompute%2Fv1beta16%2Fprojects%2Fcentos-cloud%2Fglobal%2Fimages%2Fcentos-6-v20130926
    {
      "kind": "compute#disk",
      "zone": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/zones/europe-west1-a",
      "name": "dev-tpl-0",
      "description": "Persistent boot disk created from https://www.googleapis.com/compute/v1beta16/projects/centos-cloud/global/images/centos-6-v20130926."
    }


## Configure and Boot Instance

    POST https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/zones/europe-west1-a/instances/dev-tpl-0
    {
      "kind": "compute#instance",
      "disks": [
        {
          "kind": "compute#attachedDisk",
          "boot": true,
          "type": "PERSISTENT",
          "mode": "READ_WRITE",
          "deviceName": "dev-tpl-0",
          "zone": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/zones/europe-west1-a",
          "source": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/zones/europe-west1-a/disks/dev-tpl-0"
        }
      ],
      "networkInterfaces": [
        {
          "kind": "compute#instanceNetworkInterface",
          "network": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/global/networks/default",
          "accessConfigs": [
            {
              "name": "External NAT",
              "type": "ONE_TO_ONE_NAT",
              "natIP": "8.34.220.47"
            }
          ]
        }
      ],
      "serviceAccounts": [
        {
          "kind": "compute#serviceAccount",
          "email": "default",
          "scopes": [
            "https://www.googleapis.com/auth/userinfo.email",
            "https://www.googleapis.com/auth/compute",
            "https://www.googleapis.com/auth/devstorage.full_control",
            "https://www.googleapis.com/auth/taskqueue",
            "https://www.googleapis.com/auth/bigquery",
            "https://www.googleapis.com/auth/sqlservice"
          ]
        }
      ],
      "zone": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/zones/europe-west1-a",
      "metadata": {
        "items": [
          {
            "key": "dev-tpl-seq",
            "value": "0"
          },
          {
            "key": "created",
            "value": "2013-11-25T04:15:12"
          },
          {
            "key": "modified",
            "value": "2013-11-25T04:16:14"
          }
        ]
      },
      "machineType": "https://www.googleapis.com/compute/v1beta16/projects/maklarstatistik.se:maklarstatistik-se/zones/europe-west1-a/machineTypes/g1-small",
      "name": "dev-tpl-0",
      "canIpForward": true,
      "description": "[Dev.] Template image #0 (Centos-6-v20130926).",
      "tags": {
        "items": [
          "dev-tpl",
          "dev-tpl-0",
          "mblomdahl"
        ]
      },
      "kernel": "https://www.googleapis.com/compute/v1beta16/projects/google/global/kernels/gce-no-conn-track-v20130813"
    }


# CLI Config

    gcutil --service_version="v1beta16" --project="maklarstatistik.se:maklarstatistik-se" addinstance "dev-tpl-0" \
	       --description="[Dev.] Template image #0 (Centos-6-v20130926)." --tags="dev-tpl,dev-tpl-0,mblomdahl" \
		   --zone="europe-west1-a" --machine_type="g1-small" --network="default" --external_ip_address="8.34.220.47" \
		   --metadata="dev-tpl-seq:0" --metadata="created:2013-11-25T04:15:12" --metadata="modified:2013-11-25T04:16:14" \
		   --service_account_scopes="https://www.googleapis.com/auth/userinfo.email,https://www.googleapis.com/auth/compute,https://www.googleapis.com/auth/devstorage.full_control,https://www.googleapis.com/auth/taskqueue,https://www.googleapis.com/auth/bigquery,https://www.googleapis.com/auth/sqlservice" \
		   --can_ip_forward="true" \
		   --image="https://www.googleapis.com/compute/v1beta16/projects/centos-cloud/global/images/centos-6-v20130926" \
		   --persistent_boot_disk="true"


