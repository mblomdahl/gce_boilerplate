# gce_boilerplate

## 1. Goals

Project Goals:

* Provide a transparent and maintainable general-purpose GCE workhorse
* Simplify start-up hooks configuration and streamline GAE integration
* Enable efficient teamwork with remote staff members
* Promote regular security audits


## 2. Roadmap

1. Start work on template image for dev environment, configure DNS, create Bitbucket repo.
2. Atlassian JIRA/GreenHopper setup
3. Add basic install-scripts (i.e. emacs, wget, lynx, et. al.)
4. Review service configuration and create service automation template
5. Pick suitable set of standard tools (e.g. py27, rvm, npm, grunt)
6. ...


## 3. Style Guide

_To be continued..._


## 4. Best Practices

_To be continued..._


## 5. Security

_To be continued..._


## 6. Known Issues

_To be continued..._


## 7. Change Log

_To be continued..._


## 8. Contact

For general inquiries, security issues and feature requests, contact lead developer [Mats Blomdahl](mailto:mats.blomdahl@gmail.com).



